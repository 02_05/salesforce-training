import { apiurl } from "./api.js";
window.addEventListener('load',()=>{
    getNews();
});
async function getNews(){
    //make a call to news api and get news feed
    const newsfeed=await fetch(apiurl);
    //store it in a json
    const jsonop=await newsfeed.json();
    console.log(jsonop);
    //loop through json articles and extract title,descrip,url,urltoimage
    jsonop.articles.map(arc=>{
        const title=arc.title;
        const desc=arc.description;
        const image=arc.urlToImage;
        const url=arc.url;
        //From html element
        const arcelecontainer=document.createElement("div");
        const articlelement=
                    `<style>
                    h2 {
                        font-family: Georgia, 'Times New Roman', Times, serif;
                      }
                      
                       a,
                       a:visited {
                        text-decoration: none;
                        color: inherit;
                      }
                      
                       img {
                        width: 100%;
                    </style>
        <a href="${url}">
            <h1>${title}</h1>
            <img src="${image}"></img>
            <p>${desc}</p>
        </a>`;
        arcelecontainer.innerHTML=articlelement;
        // Identify div containner
        const newcontainer=document.querySelector("#main");
        // Display the same on news.html
        newcontainer.appendChild(arcelecontainer);

    });
}